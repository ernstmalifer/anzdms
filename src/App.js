import 'whatwg-fetch';
import React, { Component } from 'react';
import './App.css';

import time from './time-01.png';


const dev = true;
const devAPI = 'http://engagisdemo.com.au/anzdms/downloads/signup-rss.xml';

let myHeaders = new Headers();

const mstochange = 60000; //1m

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      recent: ""
    }
  }

  componentWillMount(){
    if(dev){
      this.refreshData();
      this.interval = setInterval(() => {
        this.refreshData()
      }, mstochange);
    }
  }

  refreshData(){
    window.fetch(devAPI, {headers: myHeaders}).then((response) => {
      return response.text();
    })
    .then((data) => {
      const xml = (new DOMParser()).parseFromString(data,"application/xml");
      this.setState({recent: xml.getElementsByTagName("item")[0]});
      window.localStorage.setItem('xml', (new XMLSerializer()).serializeToString(xml));
    })
    .catch((e) => {
      this.getLocalStorage();
    })
  }

  getLocalStorage(){
    const xml = (new DOMParser()).parseFromString(window.localStorage.getItem('xml'), "application/xml");
    this.setState({recent: xml.getElementsByTagName("item")[0]});
  }

  render() {

    let timeElapsed, placeName, postalCode, adminName1, adminCode1, countryCode;

    if(this.state.recent !== ""){

      const namespace = 'http://honcho.com/registration/1.0';
      timeElapsed = this.state.recent.getElementsByTagName('timeElapsed')[0].innerHTML;

      placeName = this.state.recent.getElementsByTagNameNS(namespace, 'placeName')[0].innerHTML;
      postalCode = this.state.recent.getElementsByTagNameNS(namespace, 'postalCode')[0].innerHTML;
      adminName1 = this.state.recent.getElementsByTagNameNS(namespace, 'adminName1')[0].innerHTML;
      adminCode1 = this.state.recent.getElementsByTagNameNS(namespace, 'adminCode1')[0].innerHTML;
      countryCode = this.state.recent.getElementsByTagNameNS(namespace, 'countryCode')[0].innerHTML;
    }

    return (
      <div className="App">
        <h1 className="head1">A new business just started in</h1>
        <h1 className="businessLocation">{placeName}, {adminCode1}</h1>
        <h3 className="timeElapsed"><img src={time} style={{width: '20px', position: 'relative', top: '3px'}}/> {timeElapsed}</h3>
        {/*{timeElapsed}<br />
        {placeName}<br />
        {postalCode}<br />
        {adminName1}<br />
        {adminCode1}<br />
        {countryCode}<br />*/}
        <h1 className="head2">One day business set-up online</h1>
      {/*<br />
      <button onClick={this.getLocalStorage.bind(this)}>Get</button>*/}
      </div>
    );
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

export default App;